# coordinators

This project presents some differences with the Soroush's coordinator.
My ```CoordinatorDelegate``` have an attribute called ```rootViewController``` instead of the ```childCoordinators```.
The idea is set the ```rootViewController``` when the application flow needs to create a new "branch" in the navigation controller.

Imagine that your application have an authantication flow, that can be called any time... and this authantication flow has 2 or more steps. And at the end of the auth, you need to dismiss all the flow and back to the exactly view controller that called the auth.
All you need to do is set the ```rootViewController``` as that view controller, and implement a method in the coordinator of this view controller that will call ```navigationController.popToViewController(rootViewController, animated: true)``` to dismiss your coordinator flow, and back to the main navigation.

With this approach you eliminate the need to use the ```childCoordinators```. 


