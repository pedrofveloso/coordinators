//
//  CoordinatorDelegate.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 16/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

protocol CoordinatorDelegate: AnyObject {
    var rootViewController: UIViewController? { get set }
    var navigationController: UINavigationController { get set }
    func start()
    
    init(nav: UINavigationController, root: UIViewController?)
}
