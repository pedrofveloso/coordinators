//
//  AppCoordinator.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 16/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

protocol AppCoordinatorDelegate: AnyObject {
    func start()
    var current: CoordinatorDelegate? { get }
}

class AppCoordinator {
    
    private enum FlowType {
        case login, main, deeplink(link: String)
    }
    
    private var navigationController: UINavigationController
    
    private var currentCoordinator: CoordinatorDelegate?
    
    var current: CoordinatorDelegate? {
        return currentCoordinator
    }
    
    init(navigation: UINavigationController) {
        navigationController = navigation
    }
    
    private func coordinator(for flow: AppCoordinator.FlowType) {
        switch flow {
        case .login:
            createLoginCoordinator()
        case .main:
            createMainCoordinator()
        case .deeplink(let link):
            createDeeplinkCoordinator(forDeeplink: link)
        }
    }
    
    private var flow: FlowType {
        //logic to define which flow will be executed
        return .main
    }
    
    private func createLoginCoordinator() {
        //...
        currentCoordinator = LoginCoordinator(nav: navigationController, root: nil)
        currentCoordinator?.start()
    }
    
    private func createMainCoordinator() {
        //...
        currentCoordinator = MainCoordinator(nav: navigationController, root: nil)
        currentCoordinator?.start()
        
    }
    
    private func createDeeplinkCoordinator(forDeeplink deeplink: String) {
        //...
    }
}

extension AppCoordinator: AppCoordinatorDelegate {
    func start() {
        coordinator(for: flow)
    }
}
