//
//  MainCoordinator.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 17/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

protocol MainCoordinatorDelegate: AnyObject {
    func login()
    func genericScreen()
}

class MainCoordinator: CoordinatorDelegate {
    var rootViewController: UIViewController?
    
    var navigationController: UINavigationController
    
    func start() {
        let viewController = MainViewController(nibName: "\(MainViewController.self)", bundle: nil)
        viewController.coordinator = self
        
        if rootViewController == nil {
            rootViewController = viewController
        }
        
        navigationController.pushViewController(viewController, animated: true)
    }
    
    required init(nav: UINavigationController, root: UIViewController?) {
        navigationController = nav
        rootViewController = root
    }
    
    deinit {
        print("main coordinator - deinit")
    }
}

extension MainCoordinator: MainCoordinatorDelegate {
    func login() {
        let coordinator = LoginCoordinator(nav: navigationController, root: rootViewController)
        coordinator.start()
    }
    
    func genericScreen() {
        let coordinator = GenericCoordinator(nav: navigationController, root: rootViewController)
        coordinator.start()
    }
    
}
