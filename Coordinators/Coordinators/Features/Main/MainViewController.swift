//
//  MainViewController.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 17/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    var coordinator: MainCoordinatorDelegate?
    
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.setTitle("Push login flow", for: .normal)
        }
    }
    
    @IBOutlet weak var genericScreenButton: UIButton! {
        didSet {
            genericScreenButton.setTitle("Push generic screen", for: .normal)
        }
    }
    
    @IBAction func login() {
        coordinator?.login()
    }
    
    @IBAction func genericScreen() {
        coordinator?.genericScreen()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("main view controller - deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
