//
//  GenericCoordinator.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 17/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

protocol GenericCoordinatorProtocol: AnyObject {
    func pushAnotherGenericScreen()
    func close()
}

class GenericCoordinator: CoordinatorDelegate {
    var rootViewController: UIViewController?
    
    var navigationController: UINavigationController
    
    func start() {
        let viewController = GenericViewController(nibName: "\(GenericViewController.self)", bundle: nil)
        viewController.coordinator = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    required init(nav: UINavigationController, root: UIViewController?) {
        navigationController = nav
        rootViewController = root
    }
}

extension GenericCoordinator: GenericCoordinatorProtocol {
    func pushAnotherGenericScreen() {
        guard let rootViewController = rootViewController else { return }
        let coordinator = GenericCoordinator(nav: navigationController, root: rootViewController)
        coordinator.start()
    }
    
    func close() {
        guard let rootViewController = rootViewController else { return }
        navigationController.popToViewController(rootViewController, animated: true)
    }
}
