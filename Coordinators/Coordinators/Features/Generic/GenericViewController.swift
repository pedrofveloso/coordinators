//
//  GenericViewController.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 17/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

///Represents a generic screen used to push into navigation controller
class GenericViewController: UIViewController {
    
    var coordinator: GenericCoordinatorProtocol?
    
    @IBOutlet weak var pushButton: UIButton!{
        didSet {
            pushButton.setTitle("Push another screen", for: .normal)
        }
    }
    
    @IBOutlet weak var closeButton: UIButton! {
        didSet {
            closeButton.setTitle("Pop to root", for: .normal)
        }
    }
    
    @IBAction func pushAnotherScreen() {
        coordinator?.pushAnotherGenericScreen()
    }
    
    @IBAction func close() {
        coordinator?.close()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("generic view controller - deinit")
    }
}
