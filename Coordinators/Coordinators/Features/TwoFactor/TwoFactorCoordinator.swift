//
//  TwoFactorCoordinator.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 17/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

protocol TwoFactorCoordinatorDelegate: AnyObject {
    func close()
}

class TwoFactorCoordinator: CoordinatorDelegate {
    var rootViewController: UIViewController?
    
    var navigationController: UINavigationController
    
    func start() {
        let viewController = TwoFactorViewController()
        viewController.coordinator = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    required init(nav: UINavigationController, root: UIViewController?) {
        navigationController = nav
        rootViewController = root
    }
    
    deinit {
        print("two factor coordinator - deinit")
    }
    
}

extension TwoFactorCoordinator: TwoFactorCoordinatorDelegate {
    func close() {
        guard let rootViewController = rootViewController else { return }
        navigationController.popToViewController(rootViewController, animated: true)
    }
    
}
