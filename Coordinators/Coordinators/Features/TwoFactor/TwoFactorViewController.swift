//
//  TwoFactorViewController.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 17/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit
///Represents a second screen in Login flow
class TwoFactorViewController: UIViewController {
    
    var coordinator: TwoFactorCoordinatorDelegate?
    
    deinit {
        print("two factor view controller - deinit")
    }
    
    @IBOutlet weak var confirmButton: UIButton! {
        didSet {
            confirmButton.setTitle("Dismiss", for: .normal)
        }
    }
    
    @IBAction func confirm(_ sender: Any) {
        //... do something then
        coordinator?.close()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Two factor"
    }

}
