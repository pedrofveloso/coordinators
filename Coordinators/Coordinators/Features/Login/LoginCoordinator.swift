//
//  LoginCoordinator.swift
//  Coordinators
//
//  Created by Pedro Danilo Ferreira Veloso on 17/05/19.
//  Copyright © 2019 Pedro Danilo Ferreira Veloso. All rights reserved.
//

import UIKit

protocol LoginCoordinatorProtocol: AnyObject {
    func login()
}

class LoginCoordinator: CoordinatorDelegate {
    
    var rootViewController: UIViewController?
    
    var navigationController: UINavigationController
    
    func start() {
        let viewController = LoginViewController(nibName: "\(LoginViewController.self)", bundle: nil)
        viewController.coordinator = self
        
        if rootViewController == nil {
            rootViewController = viewController
        }
        
        navigationController.pushViewController(viewController, animated: true)
    }
    
    required init(nav: UINavigationController, root: UIViewController?) {
        navigationController = nav
        rootViewController = root
    }
    
    deinit {
        print("login coordinator - deinit")
    }

}

extension LoginCoordinator: LoginCoordinatorProtocol {
    func login() {
        guard let rootViewController = rootViewController else { return }
        let coordinator = TwoFactorCoordinator(nav: navigationController, root: rootViewController)
        coordinator.start()
    }
}
